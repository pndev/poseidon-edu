package poseidon.bundle.dao.auditlog;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import poseidon.bundle.service.auditlog.AuditLogDao;
import poseidon.bundle.service.auditlog.types.AuditLogEntry;
import poseidon.bundle.service.auditlog.types.AuditLogRequest;

@Repository
public class AuditLogDaoImpl implements AuditLogDao {

	@Autowired
	private AuditLogMapper mapper;

	@Override
	public List<AuditLogEntry> queryAuditLogs(AuditLogRequest req) {
		return mapper.queryAuditLogs(req);
	}

	@Override
	public void removeAuditLog(List<Integer> audId) {
		mapper.removeAuditLog(audId);
	}

	@Override
	public void insertAuditLog(AuditLogEntry entry) {
		mapper.insertAuditLog(entry);

	}
}
