package poseidon.bundle.dao.auditlog;

import java.util.List;

import poseidon.bundle.service.auditlog.types.AuditLogEntry;
import poseidon.bundle.service.auditlog.types.AuditLogRequest;

public interface AuditLogMapper {

	void removeAuditLog(List<Integer> audId);

	void insertAuditLog(AuditLogEntry entry);

	List<AuditLogEntry> queryAuditLogs(AuditLogRequest req);

}
