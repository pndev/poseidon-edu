package poseidon.bundle.dao.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import poseidon.bundle.service.security.AuthenticationDao;
import poseidon.bundle.service.security.types.AuthenticationDetails;

@Service
public class AuthenticationDaoImpl implements AuthenticationDao {

	@Autowired
	private AuthenticationMapper mapper;

	@Override
	public void storeDetails(AuthenticationDetails userDetails) {
		mapper.storeDetails(userDetails);
	}

	@Override
	public AuthenticationDetails getDetails(String userId) {
		return mapper.getDetails(userId);
	}

	@Override
	public void updateDetails(String userId, AuthenticationDetails newDet) {
		mapper.updateDetails(userId, newDet);
	}

	@Override
	public void removeDetails(String userId) {
		mapper.removeDetails(userId);
	}

	@Override
	public List<String> getOperationsForFunction(String userId, String function) {
		return mapper.getOperationsForFunction(userId, function);
	}

}
