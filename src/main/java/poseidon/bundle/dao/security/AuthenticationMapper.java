package poseidon.bundle.dao.security;

import java.util.List;

import poseidon.bundle.service.security.types.AuthenticationDetails;

public interface AuthenticationMapper {

	void storeDetails(AuthenticationDetails userDetails);

	AuthenticationDetails getDetails(String userId);

	void updateDetails(String userId, AuthenticationDetails newDet);

	void removeDetails(String userId);

	List<String> getOperationsForFunction(String userId, String function);

}
