package poseidon.bundle.dao.internalmessage;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import poseidon.bundle.service.internalmessage.InternalMessageDao;
import poseidon.bundle.service.internalmessage.types.DeleteMessageRequest;
import poseidon.bundle.service.internalmessage.types.InternalMessageData;
import poseidon.bundle.service.internalmessage.types.MessageFilter;
import poseidon.bundle.service.internalmessage.types.QueryMessageRequest;

@Repository
public class InternalMessageDaoImpl implements InternalMessageDao {

	@Autowired
	private InternalMessageMapper mapper;

	@Override
	public void insertInternalMessage(String userId, InternalMessageData data) {
		mapper.insertInternalMessage(userId, data);
	}

	@Override
	public List<InternalMessageData> queryInternalMessages(String userId, QueryMessageRequest req) {
		return mapper.queryInternalMessages(userId, req);
	}

	@Override
	public void deleteInternalMessages(String userId, DeleteMessageRequest req) {
		mapper.deleteInternalMessages(userId, req);
	}

	@Override
	public void insertFilter(String userId, MessageFilter filter) {
		mapper.insertFilter(userId, filter);
	}

	@Override
	public void removeFilter(String userId, Integer filterId) {
		mapper.removeFilter(userId, filterId);
	}

	@Override
	public void updateFilter(String userId, Integer id, MessageFilter filter) {
		mapper.updateFilter(userId, id, filter);
	}

	@Override
	public List<MessageFilter> queryFilter(String userId) {
		return mapper.queryFilter(userId);
	}
}
