package poseidon.bundle.dao.internalmessage;

import java.util.List;

import poseidon.bundle.service.internalmessage.types.DeleteMessageRequest;
import poseidon.bundle.service.internalmessage.types.InternalMessageData;
import poseidon.bundle.service.internalmessage.types.MessageFilter;
import poseidon.bundle.service.internalmessage.types.QueryMessageRequest;

public interface InternalMessageMapper {
	
	public void insertInternalMessage(String userId, InternalMessageData data);
	
	public List<InternalMessageData> queryInternalMessages(String userId, QueryMessageRequest req);
	
	public void deleteInternalMessages(String userId, DeleteMessageRequest req);
	
	public void insertFilter(String userId, MessageFilter filter);
	
	public void removeFilter(String userId, Integer filterId);
	
	public void updateFilter(String userId, Integer id, MessageFilter filter);
	
	public List<MessageFilter> queryFilter(String userId);
}
