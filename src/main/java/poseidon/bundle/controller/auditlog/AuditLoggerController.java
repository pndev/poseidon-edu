package poseidon.bundle.controller.auditlog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import poseidon.bundle.controller.auditlog.types.AuditLoggerControllerReply;
import poseidon.bundle.controller.auditlog.types.AuditLoggerControllerRequest;
import poseidon.bundle.service.auditlog.AuditLoggerFacade;
import poseidon.bundle.service.auditlog.types.AuditLogRequest;

@Controller
@RequestMapping("auditlog")
public class AuditLoggerController {

	@Autowired
	private AuditLoggerFacade facade;

	@RequestMapping(value = "query", method = RequestMethod.GET)
	public AuditLoggerControllerReply queryauditLogs(AuditLoggerControllerRequest req) {

		AuditLoggerControllerReply rep = new AuditLoggerControllerReply();

		AuditLogRequest areq = new AuditLogRequest();

		facade.queryAuditLogs(areq);

		return rep;
	}
}
