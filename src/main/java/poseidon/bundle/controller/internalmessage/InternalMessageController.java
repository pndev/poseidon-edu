package poseidon.bundle.controller.internalmessage;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import poseidon.bundle.controller.internalmessage.types.DeleteMessageControllerRequest;
import poseidon.bundle.controller.internalmessage.types.DeleteMessageControllerResponse;
import poseidon.bundle.controller.internalmessage.types.QueryMessageControllerRequest;
import poseidon.bundle.controller.internalmessage.types.QueryMessageControllerResponse;
import poseidon.bundle.controller.internalmessage.types.SendMessageControllerRequest;
import poseidon.bundle.controller.internalmessage.types.SendMessageControllerResponse;
import poseidon.bundle.controller.internalmessage.types.UpdateMessageFilterControllerRequest;
import poseidon.bundle.controller.internalmessage.types.UpdateMessageFilterControllerResponse;
import poseidon.bundle.service.internalmessage.InternalMessageFacade;
import poseidon.bundle.service.internalmessage.types.DeleteMessageRequest;
import poseidon.bundle.service.internalmessage.types.DeleteMessageResponse;
import poseidon.bundle.service.internalmessage.types.QueryMessageRequest;
import poseidon.bundle.service.internalmessage.types.QueryMessageResponse;
import poseidon.bundle.service.internalmessage.types.SendMessageRequest;
import poseidon.bundle.service.internalmessage.types.SendMessageResponse;
import poseidon.bundle.service.internalmessage.types.UpdateMessageFilterRequest;
import poseidon.bundle.service.internalmessage.types.UpdateMessageFilterResponse;

@Controller
@RequestMapping("internalmessage")
public class InternalMessageController {

	private InternalMessageFacade facade;

	@RequestMapping("send")
	public SendMessageControllerResponse sendMessage(SendMessageControllerRequest req) {
		SendMessageControllerResponse resp = new SendMessageControllerResponse();
		SendMessageRequest sreq = new SendMessageRequest();

		SendMessageResponse sresp = facade.sendMessage(sreq);

		resp.setCode(sresp.getCode());
		resp.setMessage(sresp.getMessage());

		return resp;
	}

	@RequestMapping("query")
	public QueryMessageControllerResponse queryMessages(QueryMessageControllerRequest req) {

		QueryMessageControllerResponse resp = new QueryMessageControllerResponse();
		QueryMessageRequest sreq = new QueryMessageRequest();

		QueryMessageResponse sresp = facade.queryMessages(sreq);

		resp.setCode(sresp.getCode());
		resp.setMessage(sresp.getMessage());

		return resp;

	}
	
	@RequestMapping("delete")
	public DeleteMessageControllerResponse deleteMessages(DeleteMessageControllerRequest req) {

		DeleteMessageControllerResponse resp = new DeleteMessageControllerResponse();
		DeleteMessageRequest sreq = new DeleteMessageRequest();

		DeleteMessageResponse sresp = facade.deleteMessages(sreq);

		resp.setCode(sresp.getCode());
		resp.setMessage(sresp.getMessage());

		return resp;

	}
	
	@RequestMapping("update")
	public UpdateMessageFilterControllerResponse updateMessageFilter(UpdateMessageFilterControllerRequest req) {

		UpdateMessageFilterControllerResponse resp = new UpdateMessageFilterControllerResponse();
		UpdateMessageFilterRequest sreq = new UpdateMessageFilterRequest();

		UpdateMessageFilterResponse sresp = facade.updateMessageFilter(sreq);

		resp.setCode(sresp.getCode());
		resp.setMessage(sresp.getMessage());

		return resp;

	}
}
