package poseidon.bundle.controller.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import poseidon.bundle.controller.security.types.AuthenticationRequest;
import poseidon.bundle.controller.security.types.AuthenticationResponse;
import poseidon.bundle.service.security.AuthenticationService;
import poseidon.bundle.service.security.types.AuthenticationDetails;
import poseidon.bundle.service.security.types.AuthenticationServiceRequest;

@RestController
@RequestMapping("/security")
public class LoginSecurityController {

	@Autowired
	private AuthenticationService service;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public AuthenticationResponse authenticate(AuthenticationRequest req) {
		AuthenticationResponse resp = new AuthenticationResponse();
		
		AuthenticationServiceRequest sreq = new AuthenticationServiceRequest();
		
		sreq.setUserId(req.getUserId());
		sreq.setUserToken(req.getUserToken());
		
		service.loadUserDetails(sreq);
		
		return resp;
	}
}
