package poseidon.bundle.config.security.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
public @interface Operation {

	public String value();

}
