package poseidon.bundle.config.security.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
public @interface Function {

	public String value();

}
