package poseidon.bundle.config.loginqueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import poseidon.bundle.service.loginqueue.BlockingLoginInterceptor;
import poseidon.bundle.service.loginqueue.CreditBasedIndexService;
import poseidon.bundle.service.loginqueue.IndexService;
import poseidon.bundle.service.loginqueue.IndexServiceSpi;

@Configuration
public class LoginQueueConfiguration extends WebMvcConfigurerAdapter {

	@Bean
	public BlockingLoginInterceptor loginInterceptor() {
		return new BlockingLoginInterceptor();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(loginInterceptor());
	}

	@Autowired(required = false)
	private IndexServiceSpi spi;

	@Bean
	public IndexService indexService() {

		if (spi != null) {
			IndexService serv = spi.getIndexService();
			if (serv != null)
				return serv;
		}
		return new CreditBasedIndexService();
	}
}
