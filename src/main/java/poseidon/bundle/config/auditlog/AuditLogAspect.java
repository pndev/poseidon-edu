package poseidon.bundle.config.auditlog;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import poseidon.bundle.service.auditlog.AuditLoggerProxy;

@Aspect
@Component
public class AuditLogAspect {

	@Autowired
	private AuditLoggerProxy proxy;

	@Pointcut("within(@org.springframework.stereotype.Controller *)")
	public void controllerBean() {
	}
	
	@Pointcut("within(@poseidon.bundle.config.security.annotations.Function *)")
	public void facadeBean() {
	}

	@Pointcut("execution(* *(..))")
	public void methodPointcut() {
	}

	@Pointcut("(controllerBean() || facadeBean()) && methodPointcut()")
	public void audit() {
	}
	
	@Before("audit()")
	public void beforeMethod(JoinPoint joinPoint) {

		try {
			proxy.auditLogEvent(joinPoint);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}

	}

	@AfterReturning("audit()")
	public void afterMethod(JoinPoint joinPoint) {

		try {
			proxy.auditLogEvent(joinPoint);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}

	}

}
