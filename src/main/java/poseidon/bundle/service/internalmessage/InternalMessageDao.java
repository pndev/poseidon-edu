package poseidon.bundle.service.internalmessage;

import java.util.List;

import poseidon.bundle.service.internalmessage.types.DeleteMessageRequest;
import poseidon.bundle.service.internalmessage.types.InternalMessageData;
import poseidon.bundle.service.internalmessage.types.MessageFilter;
import poseidon.bundle.service.internalmessage.types.QueryMessageRequest;

public interface InternalMessageDao {
	
	void insertInternalMessage(String userId, InternalMessageData data);
	
	List<InternalMessageData> queryInternalMessages(String userId,QueryMessageRequest req);
	
	void deleteInternalMessages(String userId,DeleteMessageRequest req);
	
	void insertFilter(String userId,MessageFilter filter);
	
	void removeFilter(String userId,Integer filterId);

	void updateFilter(String userId,Integer id, MessageFilter filter);
	
	List<MessageFilter> queryFilter(String userId);
	
}
