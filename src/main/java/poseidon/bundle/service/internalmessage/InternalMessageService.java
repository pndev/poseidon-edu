package poseidon.bundle.service.internalmessage;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import poseidon.bundle.service.internalmessage.types.DeleteMessageRequest;
import poseidon.bundle.service.internalmessage.types.DeleteMessageResponse;
import poseidon.bundle.service.internalmessage.types.FilterUpdateType;
import poseidon.bundle.service.internalmessage.types.InternalMessageData;
import poseidon.bundle.service.internalmessage.types.QueryMessageRequest;
import poseidon.bundle.service.internalmessage.types.QueryMessageResponse;
import poseidon.bundle.service.internalmessage.types.SendMessageRequest;
import poseidon.bundle.service.internalmessage.types.SendMessageResponse;
import poseidon.bundle.service.internalmessage.types.UpdateMessageFilterRequest;
import poseidon.bundle.service.internalmessage.types.UpdateMessageFilterResponse;

@Service
public class InternalMessageService {

	@Autowired
	private InternalMessageDao dao;

	public SendMessageResponse sendMessage(SendMessageRequest sreq) {

		SendMessageResponse resp = new SendMessageResponse();
		String user = SecurityContextHolder.getContext().getAuthentication().getName();
		dao.insertInternalMessage(user, sreq.getData());
		
		// TODO main logic: user existence check. message filtering logic
		
		return resp;
	}

	public QueryMessageResponse queryMessages(QueryMessageRequest sreq) {

		String user = SecurityContextHolder.getContext().getAuthentication().getName();

		List<InternalMessageData> msgs = dao.queryInternalMessages(user, sreq);
		QueryMessageResponse resp = new QueryMessageResponse();
		resp.setInternalMessages(msgs);

		return resp;
	}

	public DeleteMessageResponse deleteMessages(DeleteMessageRequest sreq) {

		String user = SecurityContextHolder.getContext().getAuthentication().getName();
		dao.deleteInternalMessages(user, sreq);
		DeleteMessageResponse resp = new DeleteMessageResponse();

		return resp;

	}

	public UpdateMessageFilterResponse updateMessageFilter(UpdateMessageFilterRequest sreq) {

		String user = SecurityContextHolder.getContext().getAuthentication().getName();

		if (sreq.getFilterUpdateType().equals(FilterUpdateType.DELETE)) {
			dao.removeFilter(user, sreq.getFilterId());
		} else if (sreq.getFilterUpdateType().equals(FilterUpdateType.CREATE)) {
			dao.insertFilter(user, sreq.getMessageFilter());
		} else if (sreq.getFilterUpdateType().equals(FilterUpdateType.UPDATE)) {
			dao.updateFilter(user, sreq.getFilterId(), sreq.getMessageFilter());
		}
		
		UpdateMessageFilterResponse resp = new UpdateMessageFilterResponse();
		
		return resp;
	}

}
