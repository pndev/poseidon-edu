package poseidon.bundle.service.internalmessage.types;

import java.util.List;

public class DeleteMessageRequest {
	private List<Integer> messages;

	public List<Integer> getMessages() {
		return messages;
	}

	public void setMessages(List<Integer> messages) {
		this.messages = messages;
	}
}
