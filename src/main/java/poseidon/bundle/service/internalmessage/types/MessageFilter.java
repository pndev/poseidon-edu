package poseidon.bundle.service.internalmessage.types;

import java.util.List;

public class MessageFilter {
	
	private Integer filterId;

	private String filterByField;

	private List<String> filteredValues;

	private FilterOperation operation;

	private String target;

	public Integer getFilterId() {
		return filterId;
	}

	public void setFilterId(Integer filterId) {
		this.filterId = filterId;
	}

	public String getFilterByField() {
		return filterByField;
	}

	public void setFilterByField(String filterByField) {
		this.filterByField = filterByField;
	}

	public FilterOperation getOperation() {
		return operation;
	}

	public void setOperation(FilterOperation operation) {
		this.operation = operation;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public List<String> getFilteredValues() {
		return filteredValues;
	}

	public void setFilteredValues(List<String> filteredValues) {
		this.filteredValues = filteredValues;
	}
}
