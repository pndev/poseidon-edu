package poseidon.bundle.service.internalmessage.types;

public class QueryMessageRequest {
	
	private String byTitleFilter;
	
	private String bySenderFilter;
	
	private String bySubjectFilter;

	public String getByTitleFilter() {
		return byTitleFilter;
	}

	public void setByTitleFilter(String byTitleFilter) {
		this.byTitleFilter = byTitleFilter;
	}

	public String getBySenderFilter() {
		return bySenderFilter;
	}

	public void setBySenderFilter(String bySenderFilter) {
		this.bySenderFilter = bySenderFilter;
	}

	public String getBySubjectFilter() {
		return bySubjectFilter;
	}

	public void setBySubjectFilter(String bySubjectFilter) {
		this.bySubjectFilter = bySubjectFilter;
	}
	
}
