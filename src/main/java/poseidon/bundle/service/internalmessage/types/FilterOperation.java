package poseidon.bundle.service.internalmessage.types;

public enum FilterOperation {
	DELETE,
	MOVE,
	SPAM,
	FORWARD
}
