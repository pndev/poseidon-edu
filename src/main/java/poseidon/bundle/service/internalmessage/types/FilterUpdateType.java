package poseidon.bundle.service.internalmessage.types;

public enum FilterUpdateType {
	DELETE,
	UPDATE,
	CREATE
}
