package poseidon.bundle.service.internalmessage.types;

public class UpdateMessageFilterRequest {

	private FilterUpdateType filterUpdateType;
	
	private Integer filterId;
	
	private MessageFilter messageFilter;

	public FilterUpdateType getFilterUpdateType() {
		return filterUpdateType;
	}

	public void setFilterUpdateType(FilterUpdateType filterUpdateType) {
		this.filterUpdateType = filterUpdateType;
	}

	public MessageFilter getMessageFilter() {
		return messageFilter;
	}

	public void setMessageFilter(MessageFilter messageFilter) {
		this.messageFilter = messageFilter;
	}

	public Integer getFilterId() {
		return filterId;
	}

	public void setFilterId(Integer filterId) {
		this.filterId = filterId;
	}
	
	
}
