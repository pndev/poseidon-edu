package poseidon.bundle.service.internalmessage.types;

import java.util.List;

import poseidon.bundle.types.BusinessMessageBean;

public class QueryMessageResponse extends BusinessMessageBean {

	private List<InternalMessageData> internalMessages;

	public void setInternalMessages(List<InternalMessageData> internalMessages) {
		this.internalMessages = internalMessages;
	}

	public List<InternalMessageData> getInternalMessages() {
		return internalMessages;
	}

}
