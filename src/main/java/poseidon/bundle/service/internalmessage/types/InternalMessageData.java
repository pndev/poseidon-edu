package poseidon.bundle.service.internalmessage.types;

import java.util.List;

public class InternalMessageData {
	
	private String subject;
	
	private String text;
	
	private List<Integer> attachedInternalDocumentId;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<Integer> getAttachedInternalDocumentId() {
		return attachedInternalDocumentId;
	}

	public void setAttachedInternalDocumentId(List<Integer> attachedInternalDocumentId) {
		this.attachedInternalDocumentId = attachedInternalDocumentId;
	}
	
}
