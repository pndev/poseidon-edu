package poseidon.bundle.service.internalmessage.types;

public class SendMessageRequest {

	private InternalMessageData data;
	
	private String toUser;

	public InternalMessageData getData() {
		return data;
	}
	
	public void setData(InternalMessageData data) {
		this.data = data;
	}

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

}
