package poseidon.bundle.service.internalmessage;

import org.springframework.beans.factory.annotation.Autowired;

import poseidon.bundle.config.security.annotations.Function;
import poseidon.bundle.config.security.annotations.Operation;
import poseidon.bundle.service.internalmessage.types.DeleteMessageRequest;
import poseidon.bundle.service.internalmessage.types.DeleteMessageResponse;
import poseidon.bundle.service.internalmessage.types.QueryMessageRequest;
import poseidon.bundle.service.internalmessage.types.QueryMessageResponse;
import poseidon.bundle.service.internalmessage.types.SendMessageRequest;
import poseidon.bundle.service.internalmessage.types.SendMessageResponse;
import poseidon.bundle.service.internalmessage.types.UpdateMessageFilterRequest;
import poseidon.bundle.service.internalmessage.types.UpdateMessageFilterResponse;

@Function("internalmessage")
public class InternalMessageFacade {

	@Autowired
	private InternalMessageService service;

	@Operation("create")
	public SendMessageResponse sendMessage(SendMessageRequest sreq) {
		return service.sendMessage(sreq);
	}

	@Operation("view")
	public QueryMessageResponse queryMessages(QueryMessageRequest sreq) {

		return service.queryMessages(sreq);
	}

	@Operation("delete")
	public DeleteMessageResponse deleteMessages(DeleteMessageRequest sreq) {

		return service.deleteMessages(sreq);
	}

	@Operation("manage")
	public UpdateMessageFilterResponse updateMessageFilter(UpdateMessageFilterRequest sreq) {
		return service.updateMessageFilter(sreq);
	}

}
