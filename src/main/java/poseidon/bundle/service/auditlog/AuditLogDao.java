package poseidon.bundle.service.auditlog;

import java.util.List;

import poseidon.bundle.service.auditlog.types.AuditLogEntry;
import poseidon.bundle.service.auditlog.types.AuditLogRequest;

public interface AuditLogDao {

	List<AuditLogEntry> queryAuditLogs(AuditLogRequest req);

	void insertAuditLog(AuditLogEntry entry);

	void removeAuditLog(List<Integer> audId);

}
