package poseidon.bundle.service.auditlog;

import org.springframework.beans.factory.annotation.Autowired;

import poseidon.bundle.service.auditlog.types.AuditLogEntry;
import poseidon.bundle.service.auditlog.types.AuditLogEvent;

public class PersistentAuditLogHandler implements AuditLogHandler {

	@Autowired
	private AuditLogDao dao;

	@Override
	public void handleAuditLog(AuditLogEvent event) {

		AuditLogEntry entry = event.getEntry();

		dao.insertAuditLog(entry);
	}

}
