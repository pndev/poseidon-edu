package poseidon.bundle.service.auditlog.types;

public class AuditLogEvent {

	private AuditLogEntry entry;
	
	public AuditLogEntry getEntry() {
		return entry;
	}
	
	public void setEntry(AuditLogEntry entry) {
		this.entry = entry;
	}
}
