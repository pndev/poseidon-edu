package poseidon.bundle.service.auditlog.types;

import java.util.List;

import poseidon.bundle.types.BusinessMessageBean;

public class AuditLogReply extends BusinessMessageBean {

	private List<AuditLogEntry> entries;

	public void setEntries(List<AuditLogEntry> entries) {
		this.entries = entries;
	}

	public List<AuditLogEntry> getEntries() {
		return entries;
	}

}
