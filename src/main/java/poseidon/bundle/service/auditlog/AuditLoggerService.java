package poseidon.bundle.service.auditlog;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import poseidon.bundle.service.auditlog.types.AuditLogEntry;
import poseidon.bundle.service.auditlog.types.AuditLogEvent;
import poseidon.bundle.service.auditlog.types.AuditLogReply;
import poseidon.bundle.service.auditlog.types.AuditLogRequest;

@Service
public class AuditLoggerService {

	@Autowired
	private AuditLogDao dao;

	private List<AuditLogHandler> handlers = new ArrayList<>();

	@PostConstruct
	public void addDefaultHandlers() {
		handlers.add(new PersistentAuditLogHandler());
	}

	public AuditLogReply getAuditLogs(AuditLogRequest req) {

		List<AuditLogEntry> entries = dao.queryAuditLogs(req);
		AuditLogReply rep = new AuditLogReply();
		rep.setEntries(entries);
		return rep;
	}

	public void handleAuditLogEvent(AuditLogEvent event) {
		for (AuditLogHandler h : handlers) {
			h.handleAuditLog(event);
		}
	}

}
