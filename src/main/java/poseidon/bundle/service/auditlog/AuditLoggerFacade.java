package poseidon.bundle.service.auditlog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import poseidon.bundle.config.security.annotations.Function;
import poseidon.bundle.config.security.annotations.Operation;
import poseidon.bundle.service.auditlog.types.AuditLogReply;
import poseidon.bundle.service.auditlog.types.AuditLogRequest;

@Component
@Function("auditlog")
public class AuditLoggerFacade {

	@Autowired
	private AuditLoggerService service;

	@Operation("view")
	public AuditLogReply queryAuditLogs(AuditLogRequest req) {

		return service.getAuditLogs(req);

	}

}
