package poseidon.bundle.service.auditlog;

import poseidon.bundle.service.auditlog.types.AuditLogEvent;

public interface AuditLogHandler {

	void handleAuditLog(AuditLogEvent event);

}
