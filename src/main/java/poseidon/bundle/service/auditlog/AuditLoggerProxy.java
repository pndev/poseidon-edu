package poseidon.bundle.service.auditlog;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import poseidon.bundle.config.security.annotations.Function;
import poseidon.bundle.config.security.annotations.Operation;
import poseidon.bundle.service.auditlog.types.AuditLogEntry;
import poseidon.bundle.service.auditlog.types.AuditLogEvent;

@Service
public class AuditLoggerProxy {

	@Autowired
	private AuditLoggerService service;

	public void auditLogEvent(JoinPoint joinPoint) throws NoSuchMethodException, SecurityException {

		AuditLogEvent event = new AuditLogEvent();
		AuditLogEntry entry = new AuditLogEntry();
		event.setEntry(entry);

		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		String methodName = signature.getMethod().getName();
		Class<?>[] parameterTypes = signature.getMethod().getParameterTypes();
		Operation op = joinPoint.getTarget().getClass().getMethod(methodName, parameterTypes)
				.getAnnotation(Operation.class);
		Function fn = joinPoint.getTarget().getClass().getAnnotation(Function.class);

		entry.setMethod(joinPoint.getTarget().getClass().getName());
		entry.setType(methodName);

		if (op != null) {
			entry.setOperation(op.value());
		}

		if (fn != null) {
			entry.setFunction(fn.value());
		}

		service.handleAuditLogEvent(event);

	}

}
