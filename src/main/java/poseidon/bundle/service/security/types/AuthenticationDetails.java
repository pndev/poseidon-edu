package poseidon.bundle.service.security.types;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public class AuthenticationDetails implements Authentication {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4279094844638652145L;
	
	private String name;
	private String credentials;
	private Map<String, List<String>> functionOperationRights = new HashMap<String, List<String>>();
	private boolean authenticated;

	public Map<String, List<String>> getFunctionOperationRights() {
		return functionOperationRights;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getCredentials() {
		return credentials;
	}

	public void setCredentials(String credentials) {
		this.credentials = credentials;
	}

	@Override
	public Object getDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getPrincipal() {
		return name;
	}

	@Override
	public boolean isAuthenticated() {
		return authenticated;
	}

	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		authenticated = isAuthenticated;
	}

	@Override
	public String getName() {
		return name;
	}

}
