package poseidon.bundle.service.security;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import poseidon.bundle.config.security.annotations.Function;
import poseidon.bundle.config.security.annotations.Operation;

@Service
@Aspect
public class AuthorizationProxy {

	@Autowired
	private AuthenticationService service;
	
	@Pointcut("within(@poseidon.bundle.config.security.annotations.Function *)")
	public void facadeBean() {
	}

	@Pointcut("execution(* *(..))")
	public void methodPointcut() {
	}

	@Before(value = "facadeBean() && methodPointcut()")
	public void interceptOperation(JoinPoint joinPoint) throws NoSuchMethodException, SecurityException {
		
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		String methodName = signature.getMethod().getName();
		Class<?>[] parameterTypes = signature.getMethod().getParameterTypes();
		Operation op = joinPoint.getTarget().getClass().getMethod(methodName, parameterTypes)
				.getAnnotation(Operation.class);
		Function fn = joinPoint.getTarget().getClass().getAnnotation(Function.class);
		
		if (op != null && fn != null) {
			if (!service.isUserAuthorized(fn.value(), op.value())) {
				throw new AccessDeniedException("Operation is not accessible");
			}
		}
		
	}

}
