package poseidon.bundle.service.security;

import java.util.List;

import poseidon.bundle.service.security.types.AuthenticationDetails;

public interface AuthenticationDao {

	void storeDetails(AuthenticationDetails userDetails);

	AuthenticationDetails getDetails(String userId);

	void updateDetails(String userId, AuthenticationDetails newDet);

	void removeDetails(String userId);

	List<String> getOperationsForFunction(String userId, String function);
}
