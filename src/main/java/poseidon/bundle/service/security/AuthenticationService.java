package poseidon.bundle.service.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import poseidon.bundle.service.security.types.AuthenticationDetails;
import poseidon.bundle.service.security.types.AuthenticationServiceRequest;

@Service
public class AuthenticationService {

	@Autowired
	private AuthenticationDao dao;

	// TODO UserDetails, Authentication Service refactor
	public void loadUserDetails(AuthenticationServiceRequest req) {

		AuthenticationDetails det = dao.getDetails(req.getUserId());

		if (det == null || !det.getCredentials().equals(req.getUserToken()))
			throw new BadCredentialsException("Invalid credentials");

		SecurityContextHolder.getContext().setAuthentication(det);
	}

	public boolean isUserAuthenticated() {
		return SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
	}

	public boolean isUserAuthorized(String function, String operation) {

		if (!isUserAuthenticated())
			return false;

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		AuthenticationDetails ad = null;
		if (auth instanceof AuthenticationDetails) {
			ad = (AuthenticationDetails) auth;
		} else {
			return false;
		}

		String user = SecurityContextHolder.getContext().getAuthentication().getName();

		List<String> ops = ad.getFunctionOperationRights().get(function);

		if (ops == null) {
			ops = dao.getOperationsForFunction(user, function);
			ad.getFunctionOperationRights().put(function, ops);
		}
		
		if (ops.contains(operation))
			return true;
		
		return false;

	}

}
