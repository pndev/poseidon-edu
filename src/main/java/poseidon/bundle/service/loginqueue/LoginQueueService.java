package poseidon.bundle.service.loginqueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginQueueService {

	@Autowired
	private IndexService service;
	
	@Autowired
	private LoginQueueRepository repository;
	
	public Integer getIndex(String user) {
		return repository.getIndex(user);
	}

	public void addToQueue(String user) {
		repository.addUserToQueue(user, service.getIndex(user));
	}

	public void removeFromQueue(String user) {
		repository.removeUserFromQueue(user);
	}
	
	public void clearQueue() {
		repository.clearQueue();
	}
	
	public void reorderQueue() {
		// TODO
	}

}
