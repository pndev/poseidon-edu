package poseidon.bundle.service.loginqueue;

public interface IndexService {
	Integer getIndex(String user);
}
