package poseidon.bundle.service.loginqueue;

public interface IndexServiceSpi {

	IndexService getIndexService();
	
}
