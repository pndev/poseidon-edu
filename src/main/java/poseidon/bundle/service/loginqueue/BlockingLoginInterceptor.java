package poseidon.bundle.service.loginqueue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class BlockingLoginInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	private LoginQueueService service;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		String user = SecurityContextHolder.getContext().getAuthentication().getName();

		Integer index = service.getIndex(user);

		if (index == null) {
			service.addToQueue(user);
		}

		if (index == 0) {
			service.removeFromQueue(user);
			return true;
		}
		
		return false;
	}

}
