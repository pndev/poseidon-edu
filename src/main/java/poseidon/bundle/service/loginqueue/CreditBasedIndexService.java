package poseidon.bundle.service.loginqueue;

import org.springframework.beans.factory.annotation.Autowired;

import poseidon.bundle.service.edu.creditindex.CreditIndexService;

public class CreditBasedIndexService implements IndexService {

	@Autowired
	private CreditIndexService service;
	
	@Override
	public Integer getIndex(String user) {
		return service.getCreditIndex(user);
	}

}
