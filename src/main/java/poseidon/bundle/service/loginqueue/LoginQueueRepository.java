package poseidon.bundle.service.loginqueue;

public interface LoginQueueRepository {

	Integer getIndex(String user);

	void addUserToQueue(String user, Integer index);

	void removeUserFromQueue(String user);

	void clearQueue();

}
